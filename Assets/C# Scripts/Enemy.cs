﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public static int HP;
    private static GameObject go;
    private static Rigidbody rb;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Bullet")
        {
            Debug.Log("Fall");
            Pool.Instanse.Back("EnemyW");
            EnemyBody(false, new Vector3(0, 0, 0), 0);
        }
        if (other.tag == "Dead")
        {
            Debug.Log("Fall");
            Pool.Instanse.Back("EnemyW");
            HP++;
            EnemyBody(false,new Vector3(0,0,0), 0);
        }
    }

    public static void EnemyBody(bool get, Vector3 vec, int power)
    {
        if (get == true)
        {
            go.transform.position = vec;
            rb.GetComponent<Rigidbody>().isKinematic = false;
        }
        else
        {
            rb.GetComponent<Rigidbody>().isKinematic = true;

        }
    }
}
