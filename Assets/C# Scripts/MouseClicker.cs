﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseClicker : MonoBehaviour
{
        private Vector3 A;
        private Rigidbody rb;


    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {

            StartCoroutine(Delay());
         
        }
    }

    IEnumerator Delay()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit))
        {
            A = new Vector3(hit.point.x, hit.point.y, hit.point.z);
            Pool.Instanse.Get("Bullet");
            Bulle.BulleBody(true, A, 150);
        }

        yield return new WaitForSeconds(2f);

    }
}
